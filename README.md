# Mapa 1
## Von Neumann vs Harvard
```plantuml
@startmindmap
* Arquitectura de computadora
	*[#FFD700] John von Neumann
		* Fisico - Matematico (1903 - 1957 Budapest, Hungria)
		* El 30 de Julio de 1945 desarrollo un programa,\nen el que consistia en el almacenado en memoria\nde los datos.
		* Hizo grandes aportaciones en el desarrollo logico\ny de la computacion.
			* Creo la arquitectura de los computadores actuales,\npropuso el uso del bit como medida de\nmemoria en los ordenadores (0 y 1)
			* Bit de paridad
			* Propuso separar el hardware del sofware
		* Participo en el diseño del primer ordenador el ENIAC,\nel cual modifico para que funcionara con\nuna sola unidad de almacenamiento
		* Su modelo esta compuesto por un procesador,\nla memoria central y las unidades de E/S, las cuales\nestan interconectadas por los buses
			* Procesador
				* Ha de disponer de una unidad aritmética y lógica (ALU)\nque pueda hacer un conjunto de operaciones. 
				* La ALU realiza una determinada operación según\nunas señales de control de entrada.\nCada operación se lleva a cabo sobre un conjunto de\ndatos y produce resultados.
			* Memoria y unidades de E/S
				* Las instrucciones que ejecuta el computador y los datos\nnecesarios para cada instrucción están almacenados\nen la memoria principal, pero para introducirlos en la memoria\nes necesario un dispositivo de entrada.
				* Una vez ejecutadas las instrucciones de un programa y generados\nunos resultados, estos resultados se deben presentar\na los usuarios y, por lo tanto, es necesario algún\ntipo de dispositivo de salida. 
			* Buses
				* Es un medio de comunicación compartido o multipunto donde\nse conectan todos los componentes que se quiere\ninterconectar.
					* Como se trata de un medio compartido, es necesario\nun mecanismo de control y acceso al bus.
		*[#limegreen] La arquitectura Von Neumann se\nbasa en tres propiedades
			* Unico espacio de memoria.
			* El contenido es accesible por posicion,\nindependientemente de que se acceda a datos\no instrucciones.
			* La ejecucion de las instrucciones es secuencial.
	*[#00FFFF] Harvard
		* Se distingue del modelo Von Neumann por la división de la memoria\nen una memoria de instrucciones y una memoria de datos.
			* De manera que el procesador puede acceder separada\ny simultáneamente a las dos memorias.
			* Debe haber un mapa de direcciones de instrucciones\ny un mapa de direcciones de datos separados. 
		* Los microcontroladores y el DSP (procesador de\nseñales digitales o digital signal processor)\nson dos tipos de computadores que utilizan\narquitectura Harvard. 
			* Microcontroladores
				* Se considera un computador dedicado, dentro de la memoria\nse almacena un solo programa que controla\nun dispositivo.
				* Estructura de\nun microcontrolador
					* Unidad de proceso
						* Procesador
							* Dispone de dos unidades funcionales\nprincipales: una unidad de control y\nuna unidad aritmética y lógica. 
						* Memoria de instrucciones
							* Donde se almacenan las instrucciones\ndel programa que debe ejecutar el\nmicrocontrolador.
						* Memoria de datos
							* Se almacenan los datos utilizados\npor los programas. 
						* Lineas de interconexion
							* Interconectan los diferentes elementos\nque forman la unidad de proceso. 
				* Dispositivos de E/S y recursos auxiliares
					* Utiliza dispositivos de E/S y otros recursos auxiliares.\nSegún la aplicación del microcontrolador, son necesarios\nunos recursos u otros.
			* DSP
				* Es un dispositivo capaz de procesar en tiempo real\nseñales procedentes de diferentes fuentes.
				* Dispone de un procesador con gran potencia de cálculo.
				* Una de las características principales de los DSP es\nque implementan muchas operaciones por hardware que\notros procesadores hacen por software
		* Usos de la arquitectura Harvard
			* No se utiliza habitualmente en computadores de propósito general,\nsino que se utiliza en computadores para aplicaciones específicas.
@endmindmap
```
# Mapa 2
## Supercomputadoras en México
```plantuml
@startmindmap
* Supercomputadoras en México
	*[#Orange] ISM-650
		* En 1958 la UNAM tuvo la primera gran computadora en latinoametica.
		* Con una memoria de 2Ks
		* Leia la informacion a travez de tarjetas perforadas, y funcionaba\ncon bulbos
			* Esto provocada un mayor espacio y energia.
		* Era capaz de procesar 1000 operaciones aritmeticas por segundo.
		* Su principal uso era en la investigacion cientifica.
	*[#lightgreen] CRAY-402
		* En 1991 la UNAM, se consolida en las grandes\nligas de la computacion al adquirirla.
		* Fue la primera supercomputadora de Ámerica Latina.
		* Equivalia a 2000 computadoras de oficina.
		* Estvo a servicio de la ciencia Mexicana\ndurante 10 años.
	*[#lightblue] KamBalam
		* Tiene una capacidad equivalente a mas\de 1300 computadoras de escritorio.
		* Se podian realizar calculos en una semana,.\nque en ese entonces serian necesarios\n25 años para resolverlos.
		* Su uso se basa en proyectos de investigacion, en areas como\nastronomia, quimica cuantica, ciencia de materiales\nnanotecnologia, diseños de farmacos, etc.
		* Puede realizar 7 billones de operaciones matematicas por segundo.
	*[#Yellow] Miztli
		* Se encuentra en la Dirección de Cómputo y de Tecnologías de Información y Comunicación (DGTIC)\nen la UNAM e inició su servicio en enero de 2013.
		* Amplio su capacidad a mas de 8344 procesadores, con los cual\nse resolveran problemas relacionados con la ciencia e investigación.
		* Tiene una velocidad aproximadamente 28 Teraflops, que significan 28 billones\nde operaciones de punto flotante en un solo segundo.
		* Estudia otros temas como: la estructura del universo, sismos, comportamiento de particulas subatomicas,\nasi como el diseño de nuevos materiales, farmacos y reactores nucleares.
		* Miztli realiza anualmente 120 proyectos de investigación tanto de la UNAM como\nde otras instituciones que solicitan el uso del supercomputo.
	*[#DDA0DD] Xiuhcoatl
		* La UNAM, Universidad Autonoma Metropolitana y Cinvestav, crearon en 2012\nel laboratorio nacional de computo de alto rendimiento del que se deriva\nel cluster hibrido de supercomputo Xiuhcoatl.
			* Para atender las necesidades de investigación y\ndesarrollar servicios de computo de alto de desempeño.
		* Se conectan las 3 instituciones a traves de una red de fibra óptica.
		* Gracias a su velocidad, es la 2da supercomputadora mas rapida de Latinoamerica.
		* Su vida estimada es de 4 años, pero con sus constantes actualizaciones la hacen funcionar.
		* En el 2012 Xiuhcoatl tenia un rendimiento de 40 Teraflops, pero actualmente es de 252 Teraflops.
	*[#FFBBCC] Supercomputadora de la BUAP
		* Considerada una de las cinco computadoras más poderosas de América Latina.
		* Tiene una capacidad de almacenamiento equivalente a mas de 5000 computadoras portatiles.
		* En tan solo 1 segundo puede ejecutar hasta 2000 millones de operaciones.
		* Una de sus pricipales aplicaciones es que puede realizar simulaciones.
		* Investigaciones cientificas
			* Se puede recrear la formación de una estrella.
			* El avance de un huracan.
			* Estructura del ADN para entender su composición.
			* Visualizar una molecula, etc.
		* Se puede utilizar tanto en el ámbito privado como de gobierno.
		* Otro de sus pricipales usos es en el sector automotriz y en aeronautica.
		* Cada 2 años, se abren convocatorias para evaluar proyectos\nque requieran un alto procesamiento numerico o almacenamiento\nde grandes cantidades de datos.
@endmindmap
```
